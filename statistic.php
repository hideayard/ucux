
<?php
if(!check_role($page,'*'))
{
  echo "<script>alert('You are not permitted!!!');window.location='home';</script>";
}

$sql = "SELECT ( SELECT count(ucux_id) FROM `ucux` WHERE YEAR(ucux_created_at) = YEAR(NOW())  AND ucux_txtform <> ''  and MONTH(ucux_created_at) = 1 ) as ucux1
,( SELECT count(ucux_id) FROM `ucux` WHERE YEAR(ucux_created_at) = YEAR(NOW())  AND ucux_txtform <> ''  and MONTH(ucux_created_at) = 2 ) as ucux2
,( SELECT count(ucux_id) FROM `ucux` WHERE YEAR(ucux_created_at) = YEAR(NOW())  AND ucux_txtform <> ''  and MONTH(ucux_created_at) = 3 ) as ucux3
,( SELECT count(ucux_id) FROM `ucux` WHERE YEAR(ucux_created_at) = YEAR(NOW())  AND ucux_txtform <> ''  and MONTH(ucux_created_at) = 4 ) as ucux4
,( SELECT count(ucux_id) FROM `ucux` WHERE YEAR(ucux_created_at) = YEAR(NOW())  AND ucux_txtform <> ''  and MONTH(ucux_created_at) = 5 ) as ucux5
,( SELECT count(ucux_id) FROM `ucux` WHERE YEAR(ucux_created_at) = YEAR(NOW())  AND ucux_txtform <> ''  and MONTH(ucux_created_at) = 6 ) as ucux6
,( SELECT count(ucux_id) FROM `ucux` WHERE YEAR(ucux_created_at) = YEAR(NOW())  AND ucux_txtform <> ''  and MONTH(ucux_created_at) = 7 ) as ucux7
,( SELECT count(ucux_id) FROM `ucux` WHERE YEAR(ucux_created_at) = YEAR(NOW())  AND ucux_txtform <> ''  and MONTH(ucux_created_at) = 8 ) as ucux8
,( SELECT count(ucux_id) FROM `ucux` WHERE YEAR(ucux_created_at) = YEAR(NOW())  AND ucux_txtform <> ''  and MONTH(ucux_created_at) = 9 ) as ucux9
,( SELECT count(ucux_id) FROM `ucux` WHERE YEAR(ucux_created_at) = YEAR(NOW())  AND ucux_txtform <> ''  and MONTH(ucux_created_at) = 10 ) as ucux10
,( SELECT count(ucux_id) FROM `ucux` WHERE YEAR(ucux_created_at) = YEAR(NOW())  AND ucux_txtform <> ''  and MONTH(ucux_created_at) = 11 ) as ucux11
,( SELECT count(ucux_id) FROM `ucux` WHERE YEAR(ucux_created_at) = YEAR(NOW())  AND ucux_txtform <> ''  and MONTH(ucux_created_at) = 12 ) as ucux12

" ;

$result = $db->rawQuery($sql);//@mysql_query($sql);

?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Statistic Overview</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Statistic</a></li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                
                
                

                <div class="col-md-6">

                    <div class="card card-warning">
                        <div class="card-header">
                          <h4 class="card-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
                              UCUX Report
                            </a>
                          </h4>
                        </div>
                        <div id="collapse3" class="panel-collapse collapse show ">
                          <div class="card-body">
                            
                                <div class="position-relative mb-4"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
                                    <canvas id="ucux-report" height="400" style="display: block; height: 200px; width: 311px;" width="622" class="chartjs-render-monitor"></canvas>
                                </div>

                                <div class="d-flex flex-row justify-content-end">
                                    <span class="mr-2">
                                        <i class="fas fa-square text-warning"></i> UCUX
                                    </span>
                                </div>

                          </div>
                        </div>
                      </div>

                </div>

                
            </div>

            <!-- /.row -->
            <div class="row col-lg-12" style="text-align:center;">
                <div class="col-lg-3 col-6">
                    <a href="home"><button type="button" class="btn btn-block btn-primary">Back</button></a>
                </div>
                <!-- ./col -->
            </div>


        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->

</div>


<script>
$(function () {
  'use strict'

  var ticksStyle = {
    fontColor: '#495057',
    fontStyle: 'bold'
  }

  var mode      = 'index'
  var intersect = true

 

  var $programChart = $('#ucux-report');
  var programChart  = new Chart($programChart, {
    type   : 'bar',
    data   : {
      labels  : ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'],
      datasets: [
        {
          backgroundColor: '#f0ad4e',
          borderColor    : '#f0ad4e',
          data           : [<?=$result[0]['ucux1']?>, <?=$result[0]['ucux2']?>, <?=$result[0]['ucux3']?>, <?=$result[0]['ucux4']?>, <?=$result[0]['ucux5']?>, <?=$result[0]['ucux6']?>, <?=$result[0]['ucux7']?>, <?=$result[0]['ucux8']?>, <?=$result[0]['ucux9']?>, <?=$result[0]['ucux10']?>, <?=$result[0]['ucux11']?>, <?=$result[0]['ucux12']?>]
        }
      ]
    },
    options: {
      maintainAspectRatio: false,
      tooltips           : {
        mode     : mode,
        intersect: intersect
      },
      hover              : {
        mode     : mode,
        intersect: intersect
      },
      legend             : {
        display: false
      },
      scales             : {
        yAxes: [{
          // display: false,
          gridLines: {
            display      : true,
            lineWidth    : '4px',
            color        : 'rgba(0, 0, 0, .6)',
            zeroLineColor: 'transparent'
          },
          ticks    : $.extend({
            beginAtZero: true,

            // Include a dollar sign in the ticks
            callback: function (value, index, values) {
              if (value >= 1000) {
                value /= 1000
                value += 'k'
              }
              // return 'MYR' + value
              return  value
            }
          }, ticksStyle)
        }],
        xAxes: [{
          display  : true,
          gridLines: {
            display: false
          },
          ticks    : ticksStyle
        }]
      }
    }
  });

});

</script>